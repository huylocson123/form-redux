import React, { Component } from 'react'
import { connect } from 'react-redux'

class TableSinhvien extends Component {
    renderSinhVien = () => {
        return this.props.arrSinhVien.map((item, index) => {
            return (
                <tr key={index} >
                    <td>{item.maSV}</td>
                    <td>{item.hoTen}</td>
                    <td>{item.soDienThoai}</td>
                    <td>{item.email}</td>
                </tr>
            )
        })
    }
    render() {
        return (
            <div className="container">
                <table className="table">
                    <thead>
                        <tr className="bg-dark text-white">
                            <th>Mã SV</th>
                            <th>Họ Tên</th>
                            <th>Số điện thoại</th>
                            <th>Email</th>
                        </tr>
                    </thead>
                    <tbody>
                        {this.renderSinhVien()}
                    </tbody>
                </table>

            </div>
        )
    }

}

const mapStateToProps = (state) => {
    return {
        arrSinhVien: state.QuanLySinhVienReducer.arrSinhVien,
    }
}

export default connect(mapStateToProps, null)(TableSinhvien)