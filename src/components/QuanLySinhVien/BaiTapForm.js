import React, { Component } from 'react'
import FormSinhVien from './FormSinhVien'
import TableSinhvien from './TableSinhvien'

export default class BaiTapForm extends Component {
    render() {
        return (
            <div className="container">
                <h3 >Bài Tập Form</h3>
                <FormSinhVien />
                <TableSinhvien />
            </div>
        )
    }
}
