import React, { Component } from "react";
import { connect } from "react-redux";

class FormSinhVien extends Component {
    state = {
        values: {
            maSV: '',
            hoTen: '',
            soDienThoai: '',
            email: '',
        },
        errors: {
            maSV: '',
            hoTen: '',
            soDienThoai: '',
            email: '',
        },
        valid: false,

    };
    handleChange = (e) => {
        let tagInput = e.target;
        let { name, value, type } = tagInput;
        let errorMessage = '';
        // kiem tra rỗng
        if (value.trim() === '') {
            errorMessage = name + ' không được bỏ trống !'
        }
        // kiem tra email
        if (type === 'email') {
            let regex = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/
            if (!regex.test(value)) {
                errorMessage = name + ' sai định dạng!'
            }
        }
        let values = { ...this.state.values, [name]: value };
        let errors = { ...this.state.errors, [name]: errorMessage };
        this.setState({
            ...this.state,
            values: values,
            errors: errors,
        }, () => {
            this.checkValid();
        })
    };

    handleSubmit = (e) => {
        // can reload submit form

        e.preventDefault();
        this.props.themSinhVien(this.state.values)
    }
    checkValid = () => {
        let valid = true;
        for (let key in this.state.errors) {
            if (this.state.errors[key] !== '' || this.state.values[key]==='') {
                valid = false;
            }
        }
      
        this.setState({
            ...this.state,
            valid: valid
        })

    }
    render() {
        return (
            <div className="container">
                <div className="card text-left">
                    <div className="card-header bg-dark text-white">
                        <h3>Thông tin sinh vien</h3>
                    </div>
                    <div className="card-body">
                        <form onSubmit={this.handleSubmit}>
                            <div className="row">
                                <div className="form-group col-6">
                                    <span>Mã SV</span>
                                    <input
                                        className="form-control"
                                        name="maSV"
                                        value={this.state.values.maSV}
                                        onChange={this.handleChange}
                                    />
                                    <span className="text-danger">{this.state.errors.maSV}</span>
                                </div>
                                <div className="form-group col-6">
                                    <span>Họ Tên</span>
                                    <input
                                        className="form-control"
                                        name="hoTen"
                                        value={this.state.values.hoTen}
                                        onChange={this.handleChange}
                                    />
                                    <span className="text-danger">{this.state.errors.hoTen}</span>
                                </div>
                            </div>
                            <div className="row">
                                <div className="form-group col-6">
                                    <span>Số điện thoại</span>
                                    <input
                                    type="number"
                                        className="form-control"
                                        name="soDienThoai"
                                        value={this.state.values.soDienThoai}
                                        onChange={this.handleChange}
                                    />
                                    <span className="text-danger">{this.state.errors.soDienThoai}</span>
                                </div>
                                <div className="form-group col-6">
                                    <span>Email</span>
                                    <input
                                        className="form-control"
                                        name="email"
                                        type="email"
                                        value={this.state.values.email}
                                        onChange={this.handleChange}
                                    />
                                    <span className="text-danger">{this.state.errors.email}</span>
                                </div>
                            </div>
                            <div className="row">
                                <div className="col-md-12 text-right mt-2">
                                    {this.state.valid ? <button className="btn btn-success" type="submit" >Thêm Sinh Viên</button>:<button className="btn btn-success" type="submit" disabled>Thêm Sinh Viên</button>}
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        );
    }
}

const mapDispatchToprops = (dispatch) => {
    return {
        themSinhVien: (sinhVien) => {
            dispatch({
                type: 'THEM_SINH_VIEN',
                payload: { sinhVien, }

            })
        }
    };
};
export default connect(null, mapDispatchToprops)(FormSinhVien)
