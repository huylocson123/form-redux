import { combineReducers } from "redux";
import { QuanLySinhVienReducer } from "./QuanLySinhVienReducer";

// Store Tổng
export const rootReducer = combineReducers({
    // nơi chứa các reducer cho nghiệp vụ
    QuanLySinhVienReducer,
})