let stateDefault = {
    arrSinhVien: [{
        maSV: 1,
        hoTen: 'huy',
        soDienThoai: '123123123',
        email: 'abc@gmail./com',

    }],

}

export const QuanLySinhVienReducer = (state = stateDefault, { type, payload }) => {
    switch (type) {
        case 'THEM_SINH_VIEN': {

            const mangSVUpdate = [...state.arrSinhVien, payload.sinhVien];
            state.arrSinhVien = mangSVUpdate;

            return { ...state }
        }
        default: return { ...state }

    };
}

